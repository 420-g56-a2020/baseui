﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class MenuOption : MonoBehaviour
{

	public Toggle pleinEcranTog, vsyncTog;

	public ResolutionItem[] resolutions;

	private int resolutionSelectionnee;

	public Text etiquetteResolution;


	public AudioMixer mixer;

	public Slider curseurVolGeneral, curseurVolMusique, curseurVolEffets;
	public Text etiquetteVolGeneral, etiquetteVolMusique, etiquetteVolEffets;

	public AudioSource sfx;

    // Start is called before the first frame update
    void Start()
    {
     	pleinEcranTog.isOn = Screen.fullScreen;

     	if(QualitySettings.vSyncCount == 0) {
     		vsyncTog.isOn = false;
     	}   else {
     		vsyncTog.isOn = true;
     	}

     	// chercher la résolution dans la liste
     	bool trouve = false;

     	for(int i = 0; i < resolutions.Length; i++) {
     		if(Screen.width == resolutions[i].horizontal && Screen.height == resolutions[i].vertical) {
     			trouve = true;

     			resolutionSelectionnee = i;
     			afficherResolution();
     		}
     	}

     	if(!trouve) {
     		etiquetteResolution.text = Screen.width.ToString() + " x " + Screen.height.ToString();
     	}

     	if(PlayerPrefs.HasKey("VolumeGeneral")) {
     		mixer.SetFloat("VolumeGeneral", PlayerPrefs.GetFloat("VolumeGeneral"));
     		curseurVolGeneral.value = PlayerPrefs.GetFloat("VolumeGeneral");
     		
     	}

     	if(PlayerPrefs.HasKey("VolumeMusique")) {
     		mixer.SetFloat("VolumeMusique", PlayerPrefs.GetFloat("VolumeMusique"));
     		curseurVolMusique.value = PlayerPrefs.GetFloat("VolumeMusique");
     		
     	}

     	if(PlayerPrefs.HasKey("VolumeEffets")) {
     		mixer.SetFloat("VolumeEffets", PlayerPrefs.GetFloat("VolumeEffets"));
     		curseurVolEffets.value = PlayerPrefs.GetFloat("VolumeEffets");
     		
     	}

     	etiquetteVolGeneral.text = (curseurVolGeneral.value + 80).ToString();
     	etiquetteVolMusique.text = (curseurVolMusique.value + 80).ToString();
     	etiquetteVolEffets.text = (curseurVolEffets.value + 80).ToString();
    }

    public void naviguerGaucheResolution() {

    	if(resolutionSelectionnee > 0) {
    		resolutionSelectionnee--;
    		afficherResolution();
    	}

    }

    public void naviguerDroiteResolution() {

    	if(resolutionSelectionnee < resolutions.Length -1) {
    		resolutionSelectionnee++;
    		afficherResolution();
    	}
    	
    }

    public void afficherResolution() {
    	etiquetteResolution.text = resolutions[resolutionSelectionnee].horizontal.ToString() + " x " + resolutions[resolutionSelectionnee].vertical.ToString();
    }

    public void appliquerOptionsGraphique() {

    	// appliquer l'option vsync
    	if(vsyncTog.isOn) {
    		QualitySettings.vSyncCount = 1;
    	} else {
    		QualitySettings.vSyncCount = 0;
    	}

    	// appliquer la resolution et l'option plein écran
    	Screen.SetResolution(resolutions[resolutionSelectionnee].horizontal, resolutions[resolutionSelectionnee].vertical, pleinEcranTog.isOn);

    }

    public void appliquerVolGeneral() {

    	etiquetteVolGeneral.text = (curseurVolGeneral.value + 80).ToString();
    	mixer.SetFloat("VolumeGeneral", curseurVolGeneral.value);

    	PlayerPrefs.SetFloat("VolumeGeneral", curseurVolGeneral.value);
    }

    public void appliquerVolMusique() {

    	etiquetteVolMusique.text = (curseurVolMusique.value + 80).ToString();
    	mixer.SetFloat("VolumeMusique", curseurVolMusique.value);

    	PlayerPrefs.SetFloat("VolumeMusique", curseurVolMusique.value);
    }

    public void appliquerVolEffets() {
    	etiquetteVolEffets.text = (curseurVolEffets.value + 80).ToString();
    	mixer.SetFloat("VolumeEffets", curseurVolEffets.value);

    	PlayerPrefs.SetFloat("VolumeEffets", curseurVolEffets.value);
    }

    public void jouerSFX() {
    	sfx.Play();
    }

    public void arreterSFX() {
    	sfx.Stop();
    }
}

[System.Serializable]
public class ResolutionItem {
	public int horizontal, vertical;


}
